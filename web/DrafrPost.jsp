<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8">
        <title>Post List</title>
        <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/5.0.0/css/bootstrap.min.css">
        <style>
            .post-item {
                display: block;
                padding: 10px;
                margin: 5px 0;
                border: 1px solid #ddd;
                border-radius: 5px;
                text-decoration: none;
                color: #333;
                transition: background-color 0.3s ease;
            }
            .post-item:hover {
                background-color: #f9f9f9;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h1>List of Draft Posts</h1>
            <c:forEach items="${listDraft}" var="pd">
                <a class="post-item" href="/postController?action=detailsDraft&postId=${pd.id}">
                    <h2>${pd.title}</h2>
                    <p><strong>Company ID:</strong> ${pd.idCompany}</p>
                    <p><strong>Salary:</strong> ${pd.salary}</p>
                    <p><strong>Start Date:</strong> ${pd.start}</p>
                    <p><strong>End Date:</strong> ${pd.end}</p>
                </a>
                <button><a href="/postController?action= ///////  add" class="btn btn-success">Post</a></button>
                <button><a href="/postController?action= ////// draft" class="btn btn-info">Delete</a></button>
            </c:forEach>
        </div>
    </body>
</html>
