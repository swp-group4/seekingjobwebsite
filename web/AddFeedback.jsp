<%-- 
    Document   : AddFeedback
    Created on : Jun 14, 2024, 6:19:18 PM
    Author     : NganPT
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Add Feedback Page</title>
    </head>
    <body>
        <form action="add" method="post">
            <table>
                <tr>
                    <td>Employee ID</td>
                    <td><input type="text" name="eId"></td>
                </tr>
                <tr>
                    <td>Company ID</td>
                    <td><input type="text" name="cId"></td>
                </tr>
                <tr>
                    <td>Description</td>
                    <td><input type="text" name="description"></td>
                </tr>
                <tr>
                    <td>Feedback Time</td>
                    <td><input type="text" name="fTime"></td>
                </tr>
                <tr>
                    <td><button type="submit">Create Feedback</button></td>
                </tr>
            </table>
        </form>
    </body>
</html>
