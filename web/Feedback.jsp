<%-- 
    Document   : Feedback
    Created on : Jun 14, 2024, 4:50:10 PM
    Author     : NganPT
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>Feedback Page</title>
    </head>
    <body>
        <table border="1px solid black">
            <tr>
                <th>Feedback ID</th>
                <th>Employee ID</th>               
                <th>Company ID</th>
                <th>Description</th>
                <th>Feedback Time</th>
            </tr>
            <c:forEach items="${listFeedback}" var="x">
                <tr>
                    <td>${x.feedbackId}</td>
                    <td>${x.employeeId}</td>               
                    <td>${x.companyId}</td>
                    <td>${x.description}</td>
                    <td>${x.feedbackTime}</td>
                    <td>
                        <a href="#">Update</a>
                        <!--<a href="delete?fId=${x.feedbackId}">Delete</a>-->
                        <a href="#" onclick="showMessage(${x.feedbackId})">Delete</a>

                    </td>
                </tr>
            </c:forEach>
        </table>
        <a href="#">Create feedback</a>
    </body>

    <script>
        function showMessage(id) {
            var option = confirm("Are you sure to delete?");
            if (option === true) {
                window.location.href = 'delete?fId='+id;
            }
        }
    </script>
</html>
