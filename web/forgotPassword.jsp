<%-- 
    Document   : forgotPassword
    Created on : Jul 2, 2024, 8:09:10 PM
    Author     : Trần Tấn Lợi-CE171879
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h3>Enter OTP and New Password</h3>
        <form action="ForgotController" method="post">
            <div>
                <label for="otp">Enter OTP:</label>
                <input type="text" id="otp" name="otp" required>
            </div>
            <div>
                <label for="confirm">Enter New Password:</label>
                <input type="password" id="confirm" name="confirm" required>
            </div>
            <div>
                <button type="submit" name="btn-ChangeForgot">Change Password</button>
            </div>
        </form>
        <form action="ForgotController" method="post">
            <button type="submit" name="btn-sendAgain">Send OTP Again</button>
        </form>
    </body>
</html>
