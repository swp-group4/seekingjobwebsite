<%-- 
    Document   : Login
    Created on : Jun 30, 2024, 9:42:08 PM
    Author     : Trần Tấn Lợi-CE171879
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Login & Forgot Password Form</title>
        <style>
            body {
                font-family: Arial, sans-serif;
                background: #f2f2f2;
                display: flex;
                justify-content: center;
                align-items: center;
                height: 100vh;
                margin: 0;
            }
            .container {
                background: white;
                padding: 30px;
                border-radius: 10px;
                box-shadow: 0 4px 8px rgba(0, 0, 0, 0.1);
                width: 300px;
            }
            .container h2 {
                text-align: center;
                margin-bottom: 20px;
                color: #333;
            }
            .form-group {
                margin-bottom: 15px;
            }
            .form-group label {
                display: block;
                margin-bottom: 5px;
                color: #666;
            }
            .form-group input {
                width: 100%;
                padding: 10px;
                border: 1px solid #ddd;
                border-radius: 5px;
                box-sizing: border-box;
            }
            .form-group input:focus {
                border-color: #007BFF;
            }
            .btn {
                width: 100%;
                padding: 10px;
                background: #007BFF;
                border: none;
                border-radius: 5px;
                color: white;
                font-size: 16px;
                cursor: pointer;
            }
            .btn:hover {
                background: #0056b3;
            }
            .forgot-password {
                text-align: center;
                margin-top: 10px;
            }
            .forgot-password a {
                color: #007BFF;
                text-decoration: none;
            }
            .forgot-password a:hover {
                text-decoration: underline;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <h2>Login</h2>
            <form action="Login"method="post">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="text" id="email" name="email" required>
                </div>
                <div class="form-group">
                    <label for="password">Password</label>
                    <input type="password" id="password" name="password" required>
                </div>
                <button type="submit" name="signIn" class="btn">Login</button>
                <div class="forgot-password">
                    <a href="/ForgotController" id="forgot-password-link">Forgot Password?</a>
                </div>
                <div style="color: red"><strong>${checkLogin}</strong></div>
            </form>
        </div>

        <div class="container" id="forgot-password-container" style="display: none;">
            <h2>Forgot Password</h2>
            <form action="ForgotController" method="post">
                <div class="form-group">
                    <label for="email">Email</label>
                    <input type="email" id="email" name="email" required>
                </div>
                <button type="submit" class="btn" name="btn-forgot">Reset Password</button>
                <div class="forgot-password">
                    <a href="Login" id="back-to-login-link">Back to Login</a>
                </div>
                
            </form>
        </div>

        <script>
            const forgotPasswordLink = document.getElementById('forgot-password-link');
            const backToLoginLink = document.getElementById('back-to-login-link');
            const loginContainer = document.querySelector('.container');
            const forgotPasswordContainer = document.getElementById('forgot-password-container');

            forgotPasswordLink.addEventListener('click', function (event) {
                event.preventDefault();
                loginContainer.style.display = 'none';
                forgotPasswordContainer.style.display = 'block';
            });

            backToLoginLink.addEventListener('click', function (event) {
                event.preventDefault();
                forgotPasswordContainer.style.display = 'none';
                loginContainer.style.display = 'block';
            });
        </script>
    </body>
</html>

