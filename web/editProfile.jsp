<%-- 
    Document   : editProfile
    Created on : Jun 14, 2024, 11:29:13 PM
    Author     : Admin
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <c:set value="${employ}" var="x" />

            <h1>Edit Profile</h1>
            <form action="UpdateProfile" method="post">
                <table>
                    <tr>
                        <td><strong>ID: </strong></td>
                        <td>    <input type="hidden" name="idEmployee" value="${x.idEmployee}" />
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Last Name: </strong></td>
                        <td><input type="text" name="lastName" value="${x.lastName}" /></td>
                    </tr>
                    <tr>
                        <td><strong>First Name: </strong></td>
                        <td><input type="text" name="firstName" value="${x.firstName}" /></td>
                    </tr>
                    <tr>
                        <td><strong>Email: </strong></td>
                        <td><input type="email" name="email" value="${x.email}" /></td>
                    </tr>
                    <tr>
                        <td><strong>Password: </strong></td>
                        <td><input type="password" name="password" value="${x.password}" /></td>
                    </tr>
                    <tr>
                        <td><strong>Phone: </strong></td>
                        <td><input type="text" name="phone" value="${x.phone}" /></td>
                    </tr>
                    <tr>
                        <td><strong>Day of Birth: </strong></td>
                        <td><input type="date" name="dob" value="${x.dob}" />
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Address: </strong></td>
                        <td><input type="text" name="address" value="${x.address}" /></td>
                    </tr>
                    <tr>
                        <td><strong>Major: </strong></td>
                        <td><input type="text" name="major" value="${x.major}" /></td>
                    </tr>
                    <tr>
                        <td><strong>Gender: </strong></td>
                        <td>
                            <select name="gender">
                                <option value="Male" ${x.gender == 'Male' ? 'selected' : ''}>Male</option>
                                <option value="Female" ${x.gender == 'Female' ? 'selected' : ''}>Female</option>
                                <option value="Other" ${x.gender == 'Other' ? 'selected' : ''}>Other</option>
                            </select>
                        </td>
                    </tr>
                    <tr>
                        <td><strong>Academic: </strong></td>
                        <td><input type="text" name="academic" value="${x.academic}" /></td>
                    </tr>
                    <tr>
                        <td colspan="2"><button type="submit">Update</button></td>
                    </tr>
                </table>
            </form>
    </body>
</html>
