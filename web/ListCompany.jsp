<%-- 
    Document   : List
    Created on : Jun 11, 2024, 6:29:35 PM
    Author     : Trần Tấn Lợi-CE171879
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table border="1px solid black">
            <tr>
                 <th>IDCompany</th>
                <th>Email</th>
                <th>Password</th>
                <th>NameCompany</th>
                <th>NameCEO</th>
                <th>VAT</th>
                <th>Location</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Logo</th>
                <th>Description</th>
            </tr>
            <c:forEach items="${listc}" var="a">
                 <tr>
                    <td>${a.idCompany}</td>
                    <td>${a.email}</td>
                    <td>${a.password}</td>
                    <td>${a.nameCompany}</td>
                    <td>${a.nameCEO}</td>
                    <td>${a.vat}</td>
                    <td>${a.location}</td>
                    <td>${a.address}</td>
                    <td>${a.phone}</td>
                    <td>${a.logo}</td>
                    <td>${a.description}</td>
                     <td>
                     <a href="CompanyController?action=account&CompanyId=${a.idCompany}">View</a>
                   
                </td>
                </tr>
             </c:forEach>


        </table>
       
    </body>
   
</html>