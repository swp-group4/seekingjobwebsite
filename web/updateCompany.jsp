<%-- 
    Document   : updateCompany
    Created on : Jun 14, 2024, 1:47:06 PM
    Author     : Trần Tấn Lợi-CE171879
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <h1>Update Company Information</h1>
        <form action="CompanyController?action=update&CompanyId=${company.idCompany}" method="post">



            <label for="email">Email:</label>
            <input type="text" id="email" name="Email" value="${company.email}" readonly/><br/>

            <label for="password">Password:</label>
            <input type="text" id="password" name="Password" value="${company.password}" required /><br/>

            <label for="nameCompany">Name of Company:</label>
            <input type="text" id="nameCompany" name="NameCompany" value="${company.nameCompany}" required/><br/>

            <label for="nameCEO">Name of CEO:</label>
            <input type="text" id="nameCEO" name="NameCEO" value="${company.nameCEO}"required /><br/>

            <label for="vat">VAT:</label>
            <input type="text" id="vat" name="Vat" value="${company.vat}" required/><br/>

            <label for="location">Location:</label>
            <input type="text" id="location" name="Location" value="${company.location}" required/><br/>

            <label for="address">Address:</label>
            <input type="text" id="address" name="Address" value="${company.address}"required /><br/>

            <label for="phone">Phone:</label>
            <input type="text" id="phone" name="Phone" value="${company.phone}"required /><br/>

            <label for="logo">Logo:</label>
            <input type="file" id="logo" name="Logo" value="${company.logo}" /><br/>

            <label for="description">Description:</label>
            <textarea id="description" name="Description" required>${company.description}</textarea><br/>

            <input type="submit" value="update" />
        </form>

    </body>
</html>
