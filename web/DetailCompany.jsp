<%-- 
    Document   : DetailCompany
    Created on : Jun 16, 2024, 7:10:32 PM
    Author     : Trần Tấn Lợi-CE171879
--%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <table border="1px solid black">
            <tr>
                <th>IDCompany</th>
                <th>Email</th>
                <th>Password</th>
                <th>NameCompany</th>
                <th>NameCEO</th>
                <th>VAT</th>
                <th>Location</th>
                <th>Address</th>
                <th>Phone</th>
                <th>Logo</th>
                <th>Description</th>
            </tr>
                <tr>
                    <td>${br.idCompany}</td>
                    <td>${br.email}</td>
                    <td>${br.password}</td>
                    <td>${br.nameCompany}</td>
                    <td>${br.nameCEO}</td>
                    <td>${br.vat}</td>
                    <td>${br.location}</td>
                    <td>${br.address}</td>
                    <td>${br.phone}</td>
                    <td>${br.logo}</td>
                    <td>${br.description}</td>
                    <a href="CompanyController?action=listUp&CompanyId=${br.idCompany}">update</a>
                    <a href="CompanyController?action=delete&CompanyId=${br.idCompany}">delete</a>
                </tr>
                
                    
        </table>

    </body>
    <script>
        function showMess(id) {
            var op = confirm('Are you sure to delete this account');
            if (op === true) {
                window.location.href = '' + id;
            }
        }
    </script>
</html>
