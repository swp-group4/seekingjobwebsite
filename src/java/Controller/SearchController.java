/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import DAO.PostDAO;
import Model.Post;

import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.SQLException;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nguyen Chi Cuong - CE171013
 */
public class SearchController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet SearchController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet SearchController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        processRequest(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {

        String textSearch = request.getParameter("searchQuery");

        String[] lo = request.getParameterValues("locations");
        String[] ma = request.getParameterValues("majors");
        String[] ty = request.getParameterValues("jobTypes");
       
        int minSalary = 0;
        int maxSalary =0;
        String filtter = Fillter(lo, ma, ty, minSalary, maxSalary, textSearch);
        request.removeAttribute("jobPosts");
        PostDAO dao = new PostDAO();
        List<Post> list = dao.getFillterSearch(filtter);

        request.setAttribute("jobPosts", list);
        List<String> majors = null;
        try {
            majors = dao.getMajor();
        } catch (SQLException ex) {
            Logger.getLogger(SearchController.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.setAttribute("majors", majors);

        List<String> locations = null;
        try {
            locations = dao.getLocation();
        } catch (SQLException ex) {
            Logger.getLogger(SearchController.class.getName()).log(Level.SEVERE, null, ex);
        }
        request.setAttribute("locations", locations);

        List<String> jobTypes = Arrays.asList("Fulltime", "Partime", "Freelancer", "Intern");
        request.setAttribute("jobTypes", jobTypes);
        request.getRequestDispatcher("home.jsp").forward(request, response);
        processRequest(request, response);
    }

    public String Fillter(String[] lo, String[] ma, String[] ty, int minSalary, int maxSalary, String search) {

        String fillter = "";
        if (lo != null) {
            for (int i = 0; i < lo.length; i++) {
                if (i < lo.length - 1) {
                    fillter += "location like N'%" + lo[i] + "%' OR ";
                } else {
                    fillter += "location like N'%" + lo[i] + "%'";
                }
            }
            if (ma != null) {
                fillter += " AND ";
            }
        }
        if (ma != null) {

            for (int i = 0; i < ma.length; i++) {
                if (i < ma.length - 1) {
                    fillter += "major like '%" + ma[i] + "%' OR ";
                } else {
                    fillter += "major like '%" + ma[i] + "%'";
                }
            }
            if (ty != null) {
                fillter += " AND ";
            }

        }

        if (ty != null) {

            for (int i = 0; i < ty.length; i++) {
                if (i < ty.length - 1) {
                    fillter += "jobTypes like '%" + ty[i] + "%' OR ";
                } else {
                    fillter += "jobTypes like '%" + ty[i] + "%'";
                }
            }
            if (search != null) {
                fillter += "AND";
            }
        }
        if (search != null) {
            fillter += "title like '%" + search + "%' OR "
                    + "description like '%" + search + "%'";
        }
        return fillter;
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
