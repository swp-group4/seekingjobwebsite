/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import DAO.PostDAO;
import Model.Post;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.sql.Date;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Nguyen Van Chien
 */
public class PostController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet PostController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet PostController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String action = request.getParameter("action");
            String postID = request.getParameter("postId");

            switch (action) {
                case "listPost":
                    listPosts(request, response);
                    break;
                case "details":
                    getPostByID(request, response, postID);
                    break;
                case "draft":
                    listDraft(request, response);
                    break;
                case "detailsDraft":
                    getDraftPostByID(request, response, postID);
                    break;
                case "deletePost":
                    deletePost(request, response, postID);
                    break;
                case "deleteDraft":
                    deleteDraftPost(request, response, postID);
                    break;
                case "createPost":
                    request.getRequestDispatcher("AddPost.jsp").forward(request, response);
                    break;
                case "addPost":
                    addPost(request, response);
                    break;
                case "updatePost":
                    getUpdatePost(request, response, postID);
                    break;
                case "updateDraftPost":
                    updateDraftPost(request, response, postID);
                    break;
                default:
                    listPosts(request, response);

            }
        } catch (SQLException ex) {
            Logger.getLogger(PostController.class.getName()).log(Level.SEVERE, null, ex);
        }

    }

    private void listPosts(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException, SQLException {

        PostDAO dao = new PostDAO();
        List<Post> list = dao.getAllPost();
        request.setAttribute("listqq", list);
        request.getRequestDispatcher("List.jsp").forward(request, response);
    }

    private void listDraft(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        PostDAO dao = new PostDAO();
        List<Post> list = dao.getAllDraftPost();
        request.setAttribute("listDraft", list);
        request.getRequestDispatcher("DrafrPost.jsp").
                forward(request, response);
    }

    private void deletePost(HttpServletRequest request, HttpServletResponse response, String postID) throws IOException {
        PostDAO dao = new PostDAO();
        dao.deletePost(postID);
        response.sendRedirect("/postController?action");
    }

    private void deleteDraftPost(HttpServletRequest request, HttpServletResponse response, String postID) throws IOException {
        PostDAO dao = new PostDAO();
        dao.deleteDraftPost(postID);
        response.sendRedirect("/postController?action=draft");
    }

    private void getPostByID(HttpServletRequest request, HttpServletResponse response, String id)
            throws ServletException, IOException {
        PostDAO dao = new PostDAO();
        Post p = dao.getPostByID(id);
        List<Post> post = new ArrayList();
        post.add(p);
        request.setAttribute("postD", post);
        request.getRequestDispatcher("postDetails.jsp").
                forward(request, response);
    }

    private void getDraftPostByID(HttpServletRequest request, HttpServletResponse response, String id)
            throws ServletException, IOException {
        PostDAO dao = new PostDAO();
        Post p = dao.getDraftPostByID(id);
        List<Post> post = new ArrayList();
        post.add(p);
        request.setAttribute("postDD", post);
        request.getRequestDispatcher("postDetailsDraft.jsp").
                forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        String postID = request.getParameter("postId");
        String update = request.getParameter("update");
        if (update.equals("update")) {

            String id = request.getParameter("id");
            String title = request.getParameter("title");

            String description = request.getParameter("description");

            String idCompany = request.getParameter("idCompany");
            String salary = request.getParameter("salary");
            String timePosted = request.getParameter("timePosted");
            SimpleDateFormat formatter = new SimpleDateFormat("dd-MMM-yyyy", Locale.ENGLISH);
            java.util.Date start = null;
            java.util.Date end = null;
            try {
                start = formatter.parse(request.getParameter("start"));
                end = formatter.parse(request.getParameter("end"));
            } catch (ParseException ex) {
                Logger.getLogger(UpdateProfile.class.getName()).log(Level.SEVERE, null, ex);
            }

            String major = request.getParameter("major");
             String location = request.getParameter("location");
             String jobTypes = request.getParameter("jobTypes");
            Post post = new Post(id, title, description, idCompany, salary, timePosted, start, end, major, jobTypes, location);
            updatePost(request, response, post);
        }
        switch (action) {
            case "details":
                saveAsDraft(request, response);
                break;
           
            default:
                request.getRequestDispatcher("Home").forward(request, response);
        }

    }

    private void addPost(HttpServletRequest request, HttpServletResponse response) {

    }

    private void saveAsDraft(HttpServletRequest request, HttpServletResponse response) {

    }

    private void getUpdatePost(HttpServletRequest request, HttpServletResponse response, String postID) throws ServletException, IOException {

        PostDAO dao = new PostDAO();
        Post p = dao.getPostByID(postID);
        request.setAttribute("pt", p);
        request.getRequestDispatcher("updatePost.jsp").forward(request, response);
    }

    private void updatePost(HttpServletRequest request, HttpServletResponse response, Post post) throws ServletException, IOException {
        PostDAO dao = new PostDAO();
        
        dao.updatePost(post);
        request.getRequestDispatcher("Home").forward(request, response);
    }

    private void updateDraftPost(HttpServletRequest request, HttpServletResponse response, String postID) throws ServletException, IOException {
        PostDAO dao = new PostDAO();
        Post p = dao.getDraftPostByID(postID);

        request.setAttribute("up", p);
        request.getRequestDispatcher("updateDraftPost.jsp").forward(request, response);

    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
