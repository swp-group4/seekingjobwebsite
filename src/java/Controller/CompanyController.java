/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/JSP_Servlet/Servlet.java to edit this template
 */
package Controller;

import DAO.CompanyDAO;
import Model.Company;
import java.io.IOException;
import java.io.PrintWriter;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServlet;
import jakarta.servlet.http.HttpServletRequest;
import jakarta.servlet.http.HttpServletResponse;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Trần Tấn Lợi-CE171879
 */
public class CompanyController extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        try ( PrintWriter out = response.getWriter()) {
            /* TODO output your page here. You may use following sample code. */
            out.println("<!DOCTYPE html>");
            out.println("<html>");
            out.println("<head>");
            out.println("<title>Servlet CompanyController</title>");
            out.println("</head>");
            out.println("<body>");
            out.println("<h1>Servlet CompanyController at " + request.getContextPath() + "</h1>");
            out.println("</body>");
            out.println("</html>");
        }
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        try {
            String action = request.getParameter("action");
            
            switch (action) {
                case "delete":
                    deleteCompany(request, response);
                    break;
                case "listUp":
                    updateCompany(request, response);
                    break;
                case "account":
                    getCompany(request, response);
                    break;
                case "listCompany":
                    
                    CompanyDAO dao = new CompanyDAO();
                    List<Company> a = dao.getAllPost();
                    request.setAttribute("listc", a);
                    request.getRequestDispatcher("ListCompany.jsp").forward(request, response);
                    
            }
        } catch (Exception ex) {
            Logger.getLogger(CompanyController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        String action = request.getParameter("action");
        
        switch (action) {
            case "delete":
                deleteCompany(request, response);
                break;
            case "account":
                getCompany(request, response);
                break;
            case "update":
                update(request, response);
                break;
            
        }
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

    private void deleteCompany(HttpServletRequest request, HttpServletResponse response) {
        
        try {
            String id = request.getParameter("CompanyId");
            CompanyDAO dao = new CompanyDAO();
            dao.deleteCompany(id);
            response.sendRedirect("Home");
        } catch (Exception ex) {
            Logger.getLogger(CompanyController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void updateCompany(HttpServletRequest request, HttpServletResponse response) {
        
        try {
            String id = request.getParameter("CompanyId");
            CompanyDAO dao = new CompanyDAO();
            Company us = dao.getUser(id);
            request.setAttribute("company", us);
            request.getRequestDispatcher("updateCompany.jsp").forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CompanyController.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
    
    private void getCompany(HttpServletRequest request, HttpServletResponse response) {
        try {
            String Company = request.getParameter("CompanyId");
            CompanyDAO dao = new CompanyDAO();
            Company a = dao.getUser(Company);
            request.setAttribute("br", a);
            request.getRequestDispatcher("DetailCompany.jsp").
                    forward(request, response);
        } catch (Exception ex) {
            Logger.getLogger(CompanyController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void update(HttpServletRequest request, HttpServletResponse response) {
        
        try {
            String sid = request.getParameter("CompanyId"); // Get sid as a string
            
            String mail = request.getParameter("Email");
            String pass = request.getParameter("Password");
            String name = request.getParameter("NameCompany");
            String nCEO = request.getParameter("NameCEO");
            String v = request.getParameter("Vat");
            String lo = request.getParameter("Location");
            String add = request.getParameter("Address");
            String dt = request.getParameter("Phone");
            String Lg = request.getParameter("Logo");
            String des = request.getParameter("Description");
//            System.out.println("Email: " + mail);
//            System.out.println("Password: " + pass);
//            System.out.println("NameCompany: " + name);
//            System.out.println("NameCEO: " + nCEO);
//            System.out.println("Vat: " + v);
//            System.out.println("Location: " + lo);
//            System.out.println("Address: " + add);
//            System.out.println("Phone: " + dt);
//            System.out.println("Logo: " + Lg);
//            System.out.println("Description: " + des);
            Company newCompany = new Company(sid, mail, pass, name, nCEO, v, lo, add, dt, Lg, des);
            CompanyDAO dao = new CompanyDAO();
            Company ne = dao.update(sid, newCompany);
            if (ne == null) {
                getCompany(request, response);
            } else {
                getCompany(request, response);
            }
            
        } catch (Exception ex) {
            Logger.getLogger(CompanyController.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
}
