/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import Model.Account;
import Model.Company;
import java.math.BigInteger;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Trần Tấn Lợi-CE171879
 */
public class AccountDAO {

    Connection conn;

    public AccountDAO() throws Exception {
        try {
           conn = new DBConnect.DBConnection().connect();
        }catch (SQLException ex) {
            Logger.getLogger(CompanyDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    public String getMd5(String input) {
        try {

            // Static getInstance method is called with hashing MD5
            MessageDigest md = MessageDigest.getInstance("MD5");

            // digest() method is called to calculate message digest
            // of an input digest() return array of byte
            byte[] messageDigest = md.digest(input.getBytes());

            // Convert byte array into signum representation
            BigInteger no = new BigInteger(1, messageDigest);

            // Convert message digest into hex value
            String hashtext = no.toString(16);
            while (hashtext.length() < 32) {
                hashtext = "0" + hashtext;
            }
            return hashtext;
        } // For specifying wrong message digest algorithms
        catch (NoSuchAlgorithmException e) {
            throw new RuntimeException(e);
        }
    }

    public Account checkLogin(String email, String password) {
        Account staff = null;
        try {
            PreparedStatement ps = conn.prepareStatement("select * from Account where Email = ? and Password = ? ");
            ps.setString(1, email);
            ps.setString(2, getMd5(password));
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                staff = new Account(rs.getString("AccountID"),
                        rs.getString("Email"), rs.getString("Password"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return staff;
    }

    public Company loginCompany(String email, String password) {
        Company cus = null;
        try {
            PreparedStatement ps = conn.prepareStatement("select * from Company where Email = ? and Password = ? ");
            ps.setString(1, email);
            ps.setString(2, getMd5(password));
            ResultSet rs = ps.executeQuery();
            while (rs.next()) {
                return cus = new Company(rs.getString(1), // IdPost
                        rs.getString(2), // Title
                        rs.getString(3), // Description
                        rs.getString(4), // IdCompany
                        rs.getString(5), // Salary
                        rs.getString(6), // TimePosted
                        rs.getString(7), // Start
                        rs.getString(8), // End
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11)// Major
                );
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return cus;
    }

    public Account checkMail(String email) {
        Account staff = null;
        try {
            PreparedStatement ps = conn.prepareStatement("select * from Account where Email = ?");
            ps.setString(1, email);
            ResultSet rs = ps.executeQuery();
            if (rs.next()) {
                staff = new Account(rs.getString("AccountID"),
                        rs.getString("Email"), rs.getString("Password"));
            }
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return staff;
    }

    public String forgotPassword(String confirm, String email) {
        int count = 0;
        try {
            PreparedStatement ps = conn.prepareStatement("UPDATE Account SET Password = ? where Email = ? ");
            ps.setString(1, getMd5(confirm));
            ps.setString(2, email);
            count = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count == 0) ? null : confirm;
    }
     public String changePassword(String confirm, String id) {
        int count = 0;
        try {
            PreparedStatement ps = conn.prepareStatement("UPDATE Account SET Password = ? where AccountId = ? ");
            ps.setString(1, getMd5(confirm));
            ps.setString(2, id);
            count = ps.executeUpdate();
        } catch (SQLException ex) {
            Logger.getLogger(AccountDAO.class.getName()).log(Level.SEVERE, null, ex);
        }
        return (count == 0) ? null : confirm;
    }
}
