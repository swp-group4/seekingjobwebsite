/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DBConnect.DBConnection;
import Model.Post;
import java.sql.Connection;
<<<<<<< HEAD
import java.sql.Date;
=======
>>>>>>> afbe72abc743a92e73350a2a1c5ba532499ee510
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Nguyen Van Chien Ce170237
 */
public class PostDAO {

    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public List<Post> getAllPost() throws SQLException {
        List<Post> list = new ArrayList<>();

        String query = "select * from ListPost";
        try {

            conn = new DBConnect.DBConnection().connect();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Post(
                        rs.getString(1), // IdPost
                        rs.getString(2), // Title
                        rs.getString(3), // Description
                        rs.getString(4), // IdCompany
                        rs.getString(5), // Salary
                        rs.getString(6), // TimePosted
                        rs.getDate(7), // Start
                        rs.getDate(8), // End
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11)
                ));
            }
        } catch (Exception e) {
        }
        conn.close();
        return list;

    }

    public List<Post> getFillterSearch(String Fillter) {
        List<Post> list = new ArrayList<>();
        String query = "";
        if (Fillter.isEmpty()) {
            query = "select * from ListPost ";
        } else {
            query = "select * from ListPost where "
                    + Fillter;
        }
        try {

            conn = new DBConnect.DBConnection().connect();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Post(
                        rs.getString(1), // IdPost
                        rs.getString(2), // Title
                        rs.getString(3), // Description
                        rs.getString(4), // IdCompany
                        rs.getString(5), // Salary
                        rs.getString(6), // TimePosted
                        rs.getDate(7), // Start
                        rs.getDate(8), // End
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11)
                // Major
                ));
            }
        } catch (Exception e) {
        }

        return list;

    }

    public List<Post> getAllDraftPost() {
        List<Post> list = new ArrayList<>();

        String query = "select * from DraftPost";
        try {

            conn = new DBConnect.DBConnection().connect();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(new Post(
                        rs.getString(1), // IdPost
                        rs.getString(2), // Title
                        rs.getString(3), // Description
                        rs.getString(4), // IdCompany
                        rs.getString(5), // Salary
                        rs.getString(6), // TimePosted
                        rs.getDate(7), // Start
                        rs.getDate(8), // End
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11)
                ));
            }
        } catch (Exception e) {
        }

        return list;

    }

    public List<String> getMajor() throws SQLException {
        List<String> list = new ArrayList<>();

        String query = "select major from ListPost";
        try {

            conn = new DBConnect.DBConnection().connect();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(rs.getString("major"));
            }
        } catch (Exception e) {
        }
        conn.close();
        return list;

    }

    public List<String> getLocation() throws SQLException {
        List<String> list = new ArrayList<>();

        String query = "select location from ListPost";
        try {

            conn = DBConnection.connect();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();

            while (rs.next()) {
                list.add(rs.getString("location"));
            }
        } catch (Exception e) {
        }
        conn.close();
        return list;

    }

//Detele 
    public void deletePost(String id) {
        String query = "delete from ListPost where id = ?";
        try {
            conn = new DBConnect.DBConnection().connect();
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            ps.executeUpdate();

        } catch (Exception e) {
        }
    }

    public void deleteDraftPost(String id) {
        String query = "delete from DraftPost where id = ?";
        try {
            conn = new DBConnect.DBConnection().connect();
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            ps.executeUpdate();

        } catch (Exception e) {
        }
    }

//-------inserPost to update
    public void inserPost(String Title, String Description,
            String IdCompany, String Salary,
            String TimePosted, String Start, String End, String Major) {

        String query = "INSERT INTO [Post] \n"
                + "VALUES (  ?, ?, ?, ?, ?, ?, ?, ?)";
        try {
            conn = new DBConnect.DBConnection().connect();
            ps = conn.prepareStatement(query);

            ps.setString(1, Title);
            ps.setString(2, Description);
            ps.setString(3, IdCompany);
            ps.setString(4, Salary);
            ps.setString(5, TimePosted);
            ps.setString(6, Start);
            ps.setString(7, End);
            ps.setString(8, Major);
            ps.executeUpdate();

        } catch (Exception e) {
        }
    }

    //------------------------------------------
    public Post getPostByID(String id) {
        String query = "select * from ListPost where id = ?";

        try {
<<<<<<< HEAD
            conn = DBConnection.connect();
=======
            conn = new DBConnect.DBConnection().connect();
>>>>>>> afbe72abc743a92e73350a2a1c5ba532499ee510
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                return new Post(
                        rs.getString(1), // IdPost
                        rs.getString(2), // Title
                        rs.getString(3), // Description
                        rs.getString(4), // IdCompany
                        rs.getString(5), // Salary
                        rs.getString(6), // TimePosted
                        rs.getDate(7), // Start
                        rs.getDate(8), // End
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11)
                );
            }
        } catch (Exception e) {
        }
        return null;
    }

    public Post getDraftPostByID(String id) {
        String query = "select * from DraftPost where id = ?";

        try {
            conn = new DBConnect.DBConnection().connect();
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();

            while (rs.next()) {
                return new Post(
                        rs.getString(1), // IdPost
                        rs.getString(2), // Title
                        rs.getString(3), // Description
                        rs.getString(4), // IdCompany
                        rs.getString(5), // Salary
                        rs.getString(6), // TimePosted
                        rs.getDate(7), // Start
                        rs.getDate(8), // End
                        rs.getString(9),
                        rs.getString(10),
                        rs.getString(11)
                );
            }
        } catch (Exception e) {
        }
        return null;
    }

<<<<<<< HEAD
    public void updatePost(Post p) {
        String id = p.getId();
        String Title = p.getTitle();

        String Description = p.getDescription();

        String IdCompany = p.getIdCompany();
        String Salary = p.getSalary();
        String TimePosted = p.getTimePosted();
        Date Start = (Date) p.getStart();
        Date End = (Date) p.getEnd();
        String Major = p.getMajor();
        String query = "UPDATE [ListPost]\n"
                + "SET [title] = ?,\n"
                + "    [description] = ?,\n"
               
                + "    [salary] = ?,\n"
                + "	[timePosted] = ?, \n"
               
                + "    [major] = ?\n"
                + "WHERE [Id] = ?; ";

        try {
            conn = DBConnection.connect();
=======
    public void updatePost(String id, String Title, String Description,
            String IdCompany, String Salary,
            String TimePosted, String Start, String End, String Major) {
        String query = "UPDATE [Post]\n"
                + "SET [title] = ?,\n"
                + "    [description] = ?,\n"
                + "    [companyID] = ?,\n"
                + "    [salary] = ?,\n"
                + "	[timePosted] = ?, \n"
                + "    [start] = ?,\n"
                + "    [end] = ?,\n"
                + "    [major] = ?\n"
                + "WHERE [postID] = ?; ";

        try {
            conn = new DBConnect.DBConnection().connect();
>>>>>>> afbe72abc743a92e73350a2a1c5ba532499ee510
            ps = conn.prepareStatement(query);

            ps.setString(1, Title);
            ps.setString(2, Description);
<<<<<<< HEAD
            
            ps.setString(3, Salary);
            ps.setString(4, TimePosted);
           
            ps.setString(5, Major);
            ps.setString(6, id);
=======
            ps.setString(3, IdCompany);
            ps.setString(4, Salary);
            ps.setString(5, TimePosted);
            ps.setString(6, Start);
            ps.setString(7, End);
            ps.setString(8, Major);
            ps.setString(9, id);
>>>>>>> afbe72abc743a92e73350a2a1c5ba532499ee510

            ps.executeUpdate();

        } catch (Exception e) {
        }
    }
<<<<<<< HEAD

    public void inserPost(String Id, String Title, String Description,
            String IDcompany, String Salary,
            String timePosted, String Start, String End, String Major, String jobTypes, String location) {

        String query = "INSERT INTO ListPost VALUES(?,?,?,?,?,?,?,?,?,?,?)";
        try {
            conn = DBConnection.connect();
            ps = conn.prepareStatement(query);

            ps.setString(1, Id);
            ps.setString(2, Title);
            ps.setString(3, Description);
            ps.setString(4, IDcompany);
            ps.setString(5, Salary);
            ps.setString(6, timePosted);
            ps.setString(7, Start);
            ps.setString(8, End);
            ps.setString(9, Major);
            ps.setString(10, jobTypes);
            ps.setString(11, location);
            ps.executeUpdate();

        } catch (Exception e) {
        }
    }

    public void inserDraftPost(String Id, String Title, String Description,
            String Idcompany, String Salary,
            String timePosted, String Start, String End, String Major, String jobTypes, String location) {

        String query = "INSERT INTO DraftPost VALUES(?,?,?,?,?,?,?,?,?,?)";
        try {
            conn = DBConnection.connect();
            ps = conn.prepareStatement(query);

            ps.setString(1, Id);
            ps.setString(2, Title);
            ps.setString(3, Description);
            ps.setString(4, Idcompany);
            ps.setString(5, Salary);
            ps.setString(6, timePosted);
            ps.setString(7, Start);
            ps.setString(8, End);
            ps.setString(9, Major);
            ps.setString(10, jobTypes);
            ps.setString(11, location);
            ps.executeUpdate();

        } catch (Exception e) {
        }
    }
=======
>>>>>>> afbe72abc743a92e73350a2a1c5ba532499ee510
}
