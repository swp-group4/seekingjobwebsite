/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package DAO;

import DBConnect.DBConnection;
import Model.Employee;
import java.sql.Connection;
import java.sql.Date;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author NganPT
 */
public class EmployeeDAO {
    
    Connection conn = null;
    PreparedStatement ps = null;
    ResultSet rs = null;

    public List<Employee> getAllEmployee() {
        List<Employee> list = new ArrayList<>();

        String query = "select * from Employee";
        try {

             conn = DBConnection.connect();
            ps = conn.prepareStatement(query);
            rs = ps.executeQuery();
            
            while (rs.next()) {
                Date date = rs.getDate("DoB");
                list.add(new Employee(rs.getString("EmployeeId"),
                        rs.getString("LastName"),
                        rs.getString("FirstName"),
                        rs.getString("Email"),
                        rs.getString("Password"),
                        rs.getString("Phone"),
                        date,
                        rs.getString("Address"),
                        rs.getString("Major"),
                        rs.getString("Gender"),
                        rs.getString("Academic")));
            }

        } catch (Exception e) {

        }

        return list;
    }
    
    public Employee getAllEmployeeById(String  id) {
       

        String query = "select * from Employee where EmployeeId =?";
        try {

            conn = DBConnection.connect();
            ps = conn.prepareStatement(query);
            ps.setString(1, id);
            rs = ps.executeQuery();
            
            if (rs.next()) {
                Date date = rs.getDate("DoB");
              return   new Employee(rs.getString("EmployeeId"),
                        rs.getString("lastName"),
                        rs.getString("firstName"),
                        rs.getString("email"),
                        rs.getString("password"),
                        rs.getString("phone"),
                        date,
                        rs.getString("address"),
                        rs.getString("major"),
                        rs.getString("gender"),
                        rs.getString("academic"));
            }

        } catch (Exception e) {
            System.out.println("Ex: " + e);
        }

        return null;
    }
    
    public void updateEmployee(Employee employee) throws SQLException, Exception {
       
        

        try {
            conn = DBConnection.connect();
            String query = "UPDATE Employee SET lastName=?, firstName=?, email=?, password=?, phone=?, DoB=?, address=?, major=?, gender=?, academic=? WHERE EmployeeId=?";
            PreparedStatement ps = conn.prepareStatement(query);
          
            
            ps.setString(1, employee.getLastName());
            ps.setString(2, employee.getFirstName());
            ps.setString(3, employee.getEmail());
            ps.setString(4, employee.getPassword());
            ps.setString(5, employee.getPhone());
            Date date = (Date) employee.getDob();
            ps.setDate(6, date);
            ps.setString(7, employee.getAddress());
            ps.setString(8, employee.getMajor());
            ps.setString(9, employee.getGender());
            ps.setString(10, employee.getAcademic());
            ps.setString(11, employee.getIdEmployee());

            int rowsUpdated = ps.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("Employee updated successfully.");
            } else {
                System.out.println("No employee updated. ID " + employee.getIdEmployee() + " not found or no changes made.");
            }
        } catch (SQLException e) {
            e.printStackTrace();
            throw e; // Re-throw the exception to be handled by the caller
        } finally {
            // Close resources in finally block
            try {
                if (ps != null) ps.close();
                if (conn != null) conn.close();
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
}
    
}
